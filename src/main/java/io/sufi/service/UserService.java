package io.sufi.service;

import io.sufi.model.User;
import io.sufi.repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUser(final String username) {
        return userRepository.findByUsername(username);
    }

    @Transactional
    public User createUser(final User user) {
        return userRepository.save(user);
    }

    @Transactional
    public User updateUser(final String username, final User user) {
        final User dbUser = getUser(username);
        // TODO: do the below with a custom null-safe bean property copier
        if (StringUtils.isNotEmpty(user.getPassword())) {
            dbUser.setPassword(user.getPassword());
        }
        if (Objects.nonNull(user.getAccountId())) {
            dbUser.setAccountId(user.getAccountId());
        }
        return userRepository.save(dbUser);
    }
}
