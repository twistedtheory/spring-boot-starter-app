package io.sufi.rest;

import io.sufi.model.User;
import io.sufi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserResource {
    private final UserService userService;

    @Autowired
    public UserResource(final UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/user")
    @ResponseBody
    public User createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    @PutMapping("/user/{username}")
    @ResponseBody
    public User updateUser(
        @PathVariable("username") final String username,
        @RequestBody User user) {
        return userService.updateUser(username, user);
    }

    @RequestMapping("/user/{username}")
    @ResponseBody
    public User getUser(@PathVariable("username") final String username) {
        return userService.getUser(username);
    }
}
