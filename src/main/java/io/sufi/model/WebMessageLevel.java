package io.sufi.model;

public enum WebMessageLevel {
    WARNING, ERROR, INFO
}
