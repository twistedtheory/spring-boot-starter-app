package io.sufi.config;

import io.sufi.rest.BaseRest;
import io.sufi.service.BaseService;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = {BaseService.class, BaseRest.class})
public class ServiceConfig {

}
